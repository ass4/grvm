/*
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
*/
#include <windows.h>

#include "openGL_tutorial.h"

//--

GLfloat window_width = 800.0;
GLfloat window_height = 800.0;
GLfloat scala = 0.05;
bool g_special_keys[128] = {false};
bool g_keys[256] = {false};
GLfloat incremento = 0.5;

//--

PT ptsCuboA[ptsSize], ptsCuboB[ptsSize], ptsCuboC[ptsSize], centroide;
PT baseCamera[3], centroCamera;
GLfloat f=65;
PT eixosA[4], eixosB[4], eixosC[4];


PT cross(PT a, PT b)   { return PT(a[1]*b[2]-a[2]*b[1],a[2]*b[0]-a[0]*b[2],a[0]*b[1]-a[1]*b[0]); }
GLfloat dot(PT a, PT b){ return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];}
ostream &operator<<(ostream &os, const PT &p) {
  return os << " (" << p[0] << ","<< p[1] << ","<< p[2] << ","<< p[3] << ") "; 
}

void initCamera(){
	centroCamera  = PT(0,0,25);

	baseCamera[2] = centroide - centroCamera;
	baseCamera[0] = cross( baseCamera[2], PT(0,1,0));
	baseCamera[1] = cross( baseCamera[0], baseCamera[2]);

	// normalizando
	baseCamera[0] = baseCamera[0]/baseCamera[0].nor();
	baseCamera[1] = baseCamera[1]/baseCamera[1].nor();
	baseCamera[2] = baseCamera[2]/baseCamera[2].nor();
	
}
void initCubo(){
	centroide = PT(0,0,0);
	GLfloat lado = 5;
	
	/*/ opcao 1
	for(int i=0;i<8;i++){
		ptsCuboA[i] = centroide + PT( lado-(lado*2*(i&1)), lado-(lado*2*((i>>1)&1)), lado-(lado*2*((i>>2)&1)) );
	}
	//*/
	
	//*/ opcao 2
	ptsCuboA[0] = PT( centroide[0]+lado,centroide[1]+lado,centroide[2]+lado);//v0
	ptsCuboA[1] = PT( centroide[0]-lado,centroide[1]+lado,centroide[2]+lado);//v1
	ptsCuboA[2] = PT( centroide[0]-lado,centroide[1]-lado,centroide[2]+lado);//v2
	ptsCuboA[3] = PT( centroide[0]+lado,centroide[1]-lado,centroide[2]+lado);//v3
	ptsCuboA[4] = PT( centroide[0]+lado,centroide[1]-lado,centroide[2]-lado);//v4
	ptsCuboA[5] = PT( centroide[0]+lado,centroide[1]+lado,centroide[2]-lado);//v5
	ptsCuboA[6] = PT( centroide[0]-lado,centroide[1]+lado,centroide[2]-lado);//v6
	ptsCuboA[7] = PT( centroide[0]-lado,centroide[1]-lado,centroide[2]-lado);//v7
	//*/
}

void myinit()
{
	eixosA[0] = PT(0,0,0);
	eixosA[1] = PT(10,0,0);
	eixosA[2] = PT(0,10,0);
	eixosA[3] = PT(0,0,10);

	initCubo();
	initCamera();
}

void myreshape (GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	window_width = (GLfloat) w;
	window_height = (GLfloat) h;
	glOrtho(0, window_width, 0, window_height, -1.0, -1.0);
}


PT RotateX(PT p, double s = 1){
	return PT(	 p[0]*1 	+ p[1]*0 		+ p[2]*0,
				 p[0]*0 	+ p[1]*COS 		+ p[2]*SIN*s,
				 p[0]*0 	- p[1]*SIN*s 	+ p[2]*COS);
}
PT RotateY(PT p, int s = 1){
	return PT(	 p[0]*COS 	+ p[1]*0 		- p[2]*SIN*s,
				 p[0]*0 	+ p[1]*1 		+ p[2]*0,
				 p[0]*SIN*s + p[1]*0 		+ p[2]*COS);
}
PT RotateZ(PT p, int s = 1){
	return PT(	 p[0]*COS 	+ p[1]*SIN*s	+ p[2]*0,
				-p[0]*SIN*s	+ p[1]*COS 		+ p[2]*0,
				 p[0]*0 	+ p[1]*0 		+ p[2]*1);
}

// Mudanca de base
void converteAtoB(){
	for(int i=0;i<ptsSize;i++){
		ptsCuboB[i] = PT( 	dot(baseCamera[0], ptsCuboA[i]-centroCamera),
							dot(baseCamera[1], ptsCuboA[i]-centroCamera),
							dot(baseCamera[2], ptsCuboA[i]-centroCamera) );
	}
	for(int i=0;i<4;i++){
		eixosB[i] = PT( 	dot(baseCamera[0], eixosA[i]-centroCamera),
							dot(baseCamera[1], eixosA[i]-centroCamera),
							dot(baseCamera[2], eixosA[i]-centroCamera) );
	}
	
}

// Projecao no plano 2d
void converteBtoC(){
	for(int i=0;i<ptsSize;i++){
		ptsCuboC[i] = ptsCuboB[i]*(f/ptsCuboB[i][2]);
	}
	for(int i=0;i<4;i++){
		eixosC[i] = eixosB[i]*(f/eixosB[i][2]);
	}
}

void mydisplay()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	
	
	converteAtoB();
	converteBtoC();
	
	glBegin(GL_LINES);
		//Y color
		glColor3f(1.0f,0.0f,0.0f);
		glVertex2f(eixosC[0][0]*scala, eixosC[0][1]*scala);
		glVertex2f(eixosC[1][0]*scala, eixosC[1][1]*scala);

		//X color
		glColor3f(0.0f,1.0f,0.0f);
		glVertex2f(eixosC[0][0]*scala, eixosC[0][1]*scala);
		glVertex2f(eixosC[2][0]*scala, eixosC[2][1]*scala);

		//Z color
		glColor3f(0.0f,0.0f,1.0f);
		glVertex2f(eixosC[0][0]*scala, eixosC[0][1]*scala);
		glVertex2f(eixosC[3][0]*scala, eixosC[3][1]*scala);

		// linhas do cubo
		glColor3f(1.0f,0.0f,1.0f);
		for(int i=0;i<12;i++){
			glVertex2f(ptsCuboC[ ll[i][0] ][0]*scala, ptsCuboC[ ll[i][0] ][1]*scala);
			glVertex2f(ptsCuboC[ ll[i][1] ][0]*scala, ptsCuboC[ ll[i][1] ][1]*scala);
		}
	glEnd();

	
	glColor3f(0.0f,0.0f,1.0f);
    glPointSize(10.0f);
	
	for(int i=0;i<ptsSize;i++){
		glBegin(GL_POINTS);
			glVertex2f(ptsCuboC[i][0]*scala, ptsCuboC[i][1]*scala);
		glEnd();
	}
	
	glColor3f(0, 0, 0);

	glFlush();
}

void myidle(){
	if(g_keys[30]){
		exit(0);
	}

	if(g_special_keys[GLUT_KEY_F5]){
		myinit();
	}

	if(g_special_keys[GLUT_KEY_RIGHT]){
		centroCamera = centroCamera + (baseCamera[0]*incremento);
	}

	if(g_special_keys[GLUT_KEY_LEFT]){
		centroCamera = centroCamera - (baseCamera[0]*incremento);
	}

	if(g_special_keys[GLUT_KEY_UP]){
		centroCamera = centroCamera + (baseCamera[1]*incremento);
	}

	if(g_special_keys[GLUT_KEY_DOWN]){
		centroCamera = centroCamera - (baseCamera[1]*incremento);
	}

	if(g_special_keys[GLUT_KEY_PAGE_UP]){
		centroCamera = centroCamera + (baseCamera[2]*incremento);
	}

	if(g_special_keys[GLUT_KEY_PAGE_DOWN]){
		centroCamera = centroCamera - (baseCamera[2]*incremento);
	}

	// rotaciona em torno do eixo y : 
	// counterclockwise 
	if(g_keys[(int)'d']){
		baseCamera[0] = RotateY(baseCamera[0], 1);
		baseCamera[2] = RotateY(baseCamera[2], 1);
	}

	//clockwise 
	if(g_keys[(int)'a']){
		baseCamera[0] = RotateY(baseCamera[0], -1);
		baseCamera[2] = RotateY(baseCamera[2], -1);
	}

	// rotaciona em torno do eixo x : 
	// counterclockwise 
	if(g_keys[(int)'s']){
		baseCamera[1] = RotateX(baseCamera[1], 1);
		baseCamera[2] = RotateX(baseCamera[2], 1);
	}
	//clockwise 
	if(g_keys[(int)'w']){
		baseCamera[1] = RotateX(baseCamera[1], -1);
		baseCamera[2] = RotateX(baseCamera[2], -1);
	}

	// rotaciona em torno do eixo x : 
	// counterclockwise 
	if(g_keys[(int)'e']){
		baseCamera[1] = RotateZ(baseCamera[1], 1);
		baseCamera[0] = RotateZ(baseCamera[0], 1);
	}
	//clockwise 
	if(g_keys[(int)'q']){
		baseCamera[1] = RotateZ(baseCamera[1], -1);
		baseCamera[0] = RotateZ(baseCamera[0], -1);
	}

	// foco
	if(g_keys[(int)'-']){
		f-=0.5;
	}

	if(g_keys[(int)'+']){
		f+=0.5;
	}
	cout<<"CentroCamera: "<<centroCamera<< " F: "<< f <<endl;
	//db(baseCamera[0].nor() _ baseCamera[1].nor() _ baseCamera[2].nor());
	Sleep(10);
	glutPostRedisplay();
}

void mykeyboard(unsigned char key, int, int) {
	g_keys[(int)key] = true;
}

void mykeyboardUp(unsigned char key, int, int) {
	g_keys[(int)key] = false;
}

void myspecial(int key, int, int) {
	g_special_keys[key] = true;
}

void myspecialUp(int key, int, int) {
	g_special_keys[key] = false;
}

int main(int argc, char **argv)
{
	//freopen("out.txt", "w", stdout);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("OpenGL");

	glutDisplayFunc(mydisplay);
	glutReshapeFunc(myreshape);
	glutSpecialFunc(myspecial);
	glutSpecialUpFunc(myspecialUp);
	glutKeyboardFunc(mykeyboard);
	glutKeyboardUpFunc(mykeyboardUp);
	glutIdleFunc(myidle);

	myinit();

	glutMainLoop();
	return 0;
}
