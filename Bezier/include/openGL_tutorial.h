/*
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/

#ifndef _OPENGL_TUTORIAL_H_
#define _OPENGL_TUTORIAL_H_

//c++ library
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <cassert>
#include <ctime>

#include <gl/glut.h>

using namespace std;

#define db(x) cerr << #x " == " << x << "\n"
#define _ << ", " <<

#define ptsSize 8

///0,5 graus
#define COS 0.99996192306417128873735516482698 //0.99984769515639123915701155881391
#define SIN 0.00872653549837393496488821397358 //0.01745240643728351281941897851632

// estrutura que representa o Ponto/Vetor
struct PT{
	GLfloat v[3];
	PT(GLfloat x = 0.0, GLfloat y = 0.0, GLfloat z = 0.0){v[0]=x;v[1]=y;v[2]=z;}
	PT operator + (const PT &p)	const { return PT(v[0]+p[0],v[1]+p[1],v[2]+p[2]); }
	PT operator - (const PT &p)	const { return PT(v[0]-p[0],v[1]-p[1],v[2]-p[2]); }
	PT operator * (GLfloat c)	const { return PT(v[0]*c,v[1]*c,v[2]*c); }
	PT operator / (GLfloat c)	const { return PT(v[0]/c,v[1]/c,v[2]/c); }
	const GLfloat & operator[](int i) const { return v[i]; }
	double nor() {return sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);}
};


// ordem em que as linhas do cubo serao desenhadas
int ll[12][2] = {
{1,0},
{0,5},
{5,6},
{6,1},

{2,3},
{3,4},
{4,7},
{7,2},

{1,2},
{0,3},
{5,4},
{6,7} };

/**
* Funcao para inicializacao do programa.
*/
void myinit();

/**
* Esta funcao vai ser chamada quando a tela for redimensionada.
* @param w Largura atual da tela.
* @param h Altura atual da tela.
*/
void myreshape (GLsizei w, GLsizei h);

/**
* Esta � a funcao responsavel por pintar os objetos na tela.
*/
void mydisplay();

#endif //_OPENGL_TUTORIAL_H_
