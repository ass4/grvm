function m = normalisepts(m)
for i = 1:length(m)
	m(1,i) = m(1,i)./m(3,i);
	m(2,i) = m(2,i)./m(3,i);
    m(3,i) = 1;
end