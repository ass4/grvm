function H = exe1(x1, x2)

[r1,c1] = size(x1);
[r2,c2] = size(x2);

%% centroid dos pontos
cent1 = mean(x1(1:2,:)')';
cent2 = mean(x2(1:2,:)')';

%% translacao do centroid para a origem
x1(1,:) = x1(1,:) - cent1(1);
x1(2,:) = x1(2,:) - cent1(2);

x2(1,:) = x2(1,:) - cent2(1);
x2(2,:) = x2(2,:) - cent2(2);

%% normalizacao dos pontos: distancia media = sqrt(2)
distMedia1 = mean(sqrt(x1(1,:).^2 + x1(2,:).^2));
distMedia2 = mean(sqrt(x2(1,:).^2 + x2(2,:).^2));

escala1 = sqrt(2)/distMedia1;
escala2 = sqrt(2)/distMedia2;

x1(1:2,:) = escala1*x1(1:2,:);
x2(1:2,:) = escala2*x2(1:2,:);

%% matriz de transformacao 1
T1 = [escala1    0       -escala1*cent1(1)
      0         escala1  -escala1*cent1(2)
      0         0       1      ];

%% matriz de transformacao 2
T2 = [escala2    0       -escala2*cent2(1)
      0         escala2  -escala2*cent2(2)
      0         0       1      ];

if (c1 == c2)
    A = zeros(2*c1,9);
    
    for n = 1:c1
        % x1_x = x1(1,n); x1_y = x1(2,n);
        % x2_x = x2(1,n); x2_y = x2(2,n);
		
        % A(2*n-1,:) = [x1_x x1_y 1 0 0 0 -x2_x*x1_x -x2_x*x1_y -x2_x];
        % A(2*n  ,:) = [0 0 0 x1_x x1_y 1 -x2_y*x1_x -x2_y*x1_y -x2_y];
		
		A(2*n-1,:) = [0 0 0 -x2(3,n)*x1(:,n)' x2(2,n)*x1(:,n)'];
        A(2*n  ,:) = [x2(3,n)*x1(:,n)' 0 0 0 -x2(1,n)*x1(:,n)'];
    end
    
    [U,D,V] = svd(A);
    
    H = reshape(V(:,9),3,3)';
end

%% Homografia final
H = inv(T2)*H*T1;