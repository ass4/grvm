% imgColor1 = imread('data/img5.jpg');
% imgColor2 = imread('data/img3.jpg');

imgColor1 = imread('images/sse1.bmp');
imgColor2 = imread('images/sse2.bmp');

% imgColor1 = imread('images/gate1.bmp');
% imgColor2 = imread('images/gate2.bmp');

% imgColor1 = imread('data/landscape-b.jpg');
% imgColor2 = imread('data/landscape-a.jpg');



imgBW1 = double(rgb2gray(imgColor1));
imgBW2 = double(rgb2gray(imgColor2));

imgColor1 = double(imgColor1);
imgColor2 = double(imgColor2);



imgBW1=imgBW1-min(imgBW1(:)) ;
imgBW1=imgBW1/max(imgBW1(:)) ;
imgBW2=imgBW2-min(imgBW2(:)) ;
imgBW2=imgBW2/max(imgBW2(:)) ;

fprintf('Computing frames and descriptors.\n') ;
[frames1,descr1,gss1,dogss1] = sift( imgBW1, 'Verbosity', 1 ) ;
[frames2,descr2,gss2,dogss2] = sift( imgBW2, 'Verbosity', 1 ) ;


% Assemble homogeneous feature coordinates for fitting of the
% homography, note that [x,y] corresponds to [col, row]

descr1=uint8(512*descr1) ;
descr2=uint8(512*descr2) ;
tic ;
matches=siftmatch( descr1, descr2 ) ;
fprintf('Matched in %.3f s\n', toc) ;


x1 = [frames1(1:2,matches(1,:)) ; ones(1,length(matches))];
x2 = [frames2(1:2,matches(2,:)) ; ones(1,length(matches))];


t = 0.5;  % Distance threshold for deciding outliers

% [H, inliers] = ransacfithomography(x1, x2, t);
% [H, inliers] = exe2(x1, x2, t);
% [H, inliers] = Ransac(x1, x2, t);

fprintf('total matched points %d\n', length(inliers)) ;

asdf = zeros(2,length(inliers));
for i = 1:length(inliers)
	asdf(:,i) = i;
end


figure(3) ; clf ;
plotmatches(imgBW1,imgBW2,x1(1:2,inliers),x2(1:2,inliers),asdf) ;
drawnow ;


fprintf('iniciando aplicacao da homografia e juncao das imagens\n') ;

InverseOfH = inv(H); %when I calculate the transformed image, i use bilinear interpolation

%at first, we need to calculate the coordinates for the four corners fo
%image1 in the coordinate system of img2
[rowsIm1, colsIm1] = size(imgBW1); 
[rowsIm2, colsIm2] = size(imgBW2);
finalLeft = 1;
finalRight = colsIm2;
finalTop = 1;
finalBot = rowsIm2;

leftTopCornerCoord = H * [1;1;1];
leftTopCornerCoord = leftTopCornerCoord / leftTopCornerCoord(3,1);
if leftTopCornerCoord(1) < finalLeft
	finalLeft = floor(leftTopCornerCoord(1));
end
if leftTopCornerCoord(2) < finalTop
	finalTop = floor(leftTopCornerCoord(2));
end

RightTopCornerCoord = H * [colsIm1;1;1];
RightTopCornerCoord = RightTopCornerCoord / RightTopCornerCoord(3,1);
if RightTopCornerCoord(1) > finalRight
	finalRight = floor(RightTopCornerCoord(1));
end
if RightTopCornerCoord(2) < finalTop
	finalTop = floor(RightTopCornerCoord(2));
end

leftBotCornerCoord = H * [1;rowsIm1;1];
leftBotCornerCoord = leftBotCornerCoord / leftBotCornerCoord(3,1);
if leftBotCornerCoord(1) < finalLeft
	finalLeft = floor(leftBotCornerCoord(1));
end
if leftBotCornerCoord(2) > finalBot
	finalBot = floor(leftBotCornerCoord(2));
end

RightBotCornerCoord = H * [colsIm1;rowsIm1;1];
RightBotCornerCoord = RightBotCornerCoord / RightBotCornerCoord(3,1);
if RightBotCornerCoord(1) > finalRight
	finalRight = floor(RightBotCornerCoord(1));
end
if RightBotCornerCoord(2) > finalBot
	finalBot = floor(RightBotCornerCoord(2));
end

mergeRows = finalBot - finalTop + 1;
mergeCols = finalRight - finalLeft + 1;
transformedImage = zeros(mergeRows, mergeCols,3);
for row = 1:mergeRows
	for col = 1: mergeCols
		currentCoord = [col+finalLeft-1;row+finalTop-1;1];
		CoordInOriImage = InverseOfH * currentCoord;
		CoordInOriImage = CoordInOriImage / CoordInOriImage(3,1);
		
		xInSrcImage = CoordInOriImage(1,1);
		yInSrcImage = CoordInOriImage(2,1);
		
		floorY = floor(yInSrcImage);
		floorX = floor(xInSrcImage);
		ceilY = ceil(yInSrcImage);
		ceilX = ceil(xInSrcImage);
		normalizedX = xInSrcImage - floorX;
		normalizedY = yInSrcImage - floorY;
		
		if (floorX >= 1 && floorY >=1 && ceilX <= colsIm1 && ceilY <= rowsIm1) 
			f00 = imgColor1(floorY,floorX,1);
			f01 = imgColor1(ceilY,floorX,1);
			f10 = imgColor1(floorY,ceilX,1);
			f11 = imgColor1(ceilY,ceilX,1);
			transformedImage(row,col,1) = f00 + normalizedX * (f10 - f00)+ ...
										normalizedY * (f01 - f00) + ...
										normalizedX*normalizedY*(f00-f10-f01+f11);
									
			f00 = imgColor1(floorY,floorX,2);
			f01 = imgColor1(ceilY,floorX,2);
			f10 = imgColor1(floorY,ceilX,2);
			f11 = imgColor1(ceilY,ceilX,2);
			transformedImage(row,col,2) = f00 + normalizedX * (f10 - f00)+ ...
										normalizedY * (f01 - f00) + ...
										normalizedX*normalizedY*(f00-f10-f01+f11);
									
			f00 = imgColor1(floorY,floorX,3);
			f01 = imgColor1(ceilY,floorX,3);
			f10 = imgColor1(floorY,ceilX,3);
			f11 = imgColor1(ceilY,ceilX,3);
			transformedImage(row,col,3) = f00 + normalizedX * (f10 - f00)+ ...
										normalizedY * (f01 - f00) + ...
										normalizedX*normalizedY*(f00-f10-f01+f11);
		end
	end
end

transformedImage(-finalTop + 2 : -finalTop + 1 + rowsIm2, -finalLeft + 2 : -finalLeft + 1 + colsIm2,:) = imgColor2;
figure;imshow(uint8(transformedImage),[]);

