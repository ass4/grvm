% Como utilizar as funcoes ransac1 e ransac2
% as matrizes 3xN que recebe é da seguinte forma:
% linha 1 = coordenada X
% linha 2 = coordenada Y
% linha 3 = fator de escala homogenea
% 
% Argumentos:
%	x1					= 3xN pontos de coordenada homogenea.
%	x2					= 3xN pontos de coordenada homogenea tal que x1<->x2.
%	t					= O limiar de distância entre o ponto de dados e o modelo
%						  usado para decidir se um ponto é um inlier ou não.

function [H,inliers] = exe2(x1, x2, t)
	% funcaoCriarModelo	= @homography2d;
    % funcaoDist			= @homogdist2d;

	funcaoCriarModelo	= @exe1;
    funcaoDist			= @distHomografia2d;
	s = 4;
	[r,c] = size(x1)
	T = c*0.9;
	N = 1000;
	%[H, inliers] = ransac1(x1, x2, funcaoCriarModelo, funcaoDist, s, t, T, N);
    [H, inliers] = ransac2(x1, x2, funcaoCriarModelo, funcaoDist, s, t);
	
% RANSAC - RANdom SAmple Consensus - Algoritmo
%
% [M, inliers] = ransac1(x1, x2, funcaoCriarModelo, funcaoDist, s, t, T)
%
% Argumentos:
%	x1					= pontos da imagem 1
%	x2					= pontos da imagem 2 (x2 = H.x1)
%	funcaoCriarModelo	= funcao do exercicio 1 - criar matriz da homografia.
%	funcaoDist			= funcao que filtra os pontos com distancia menor que o threshold t
%	s					= numero de pontos amostrados (1 - Randomly select a sample of s data points from S and instantiate the model from this subset.)
%	t					= threshold t of the model (2 - Determine the set of data points Si that are within a distance threshold t of the model. The set Si is the consensus set of the sample and defines the inliers of S.)
%	T					= threshold T (3 - If the size of Si (the number of inliers) is greater than some threshold T, re-estimate the model using all the points in Si and terminate.)
%	N					= numero de 'Trials' (4 - After N trials the largest consensus set Si is selected, and the model is re-estimated using all the points in the subset Si.)

% referencia :
% Richard Hartley and Andrew Zisserman. "Multiple View Geometry in Computer Vision"
% pp 118. Cambridge University Press, 2003, 2nd edition.

function [M, bestinliers] = ransac1(x1, x2, funcaoCriarModelo, funcaoDist, s, t, T, N)
	
	[rows, npts] = size(x1);
    largestConsensus =  0;
	bestinliers = [];
	
	for sample_count = 1:N
        
		% Gera s indices das amostras que servirao para inicializar o modelo(homografia)
		% randomicamente no intervalo de 1..npts
		ind = randsample(npts, s);
		
		%inicializa o modelo
        M = feval(funcaoCriarModelo, x1(:,ind), x2(:,ind));
		
		
        [inliers] = feval(funcaoDist, M, x1, x2, t);
        
        % numero de inliers para este modelo
        ninliers = length(inliers);
		
		warning( sprintf('numero de Inliers %d', ninliers));
		
        if ninliers > largestConsensus		% Maior quantidade de inliers até agora
            largestConsensus = ninliers;
            bestinliers = inliers;
        end
		
		if ninliers > T
			break;
		end
    end
    
	M = feval(funcaoCriarModelo, x1(:,bestinliers), x2(:,bestinliers));
    
	

	

% RANSAC - RANdom SAmple Consensus - Algoritmo - ADAPTIVE VERSION
%
% [M, inliers] = ransac1(x1, x2, funcaoCriarModelo, funcaoDist, s, t, T)
%
% Argumentos: 
% semelhante a funcao ransac1, com a diferenca que
% nao tem os parametros T, N(pois eh calculado 'dinamicamente').

% referencia :
% Richard Hartley and Andrew Zisserman. "Multiple View Geometry in Computer Vision"
% pp 121. Cambridge University Press, 2003, 2nd edition.



function [M, bestinliers] = ransac2(x1, x2, funcaoCriarModelo, funcaoDist, s, t)
	
	[rows, npts] = size(x1);
    largestConsensus =  0;
	bestinliers = [];
	sample_count = 0;
	p = 0.99;
	N = 1
	
	while N > sample_count
        
		% Gera s indices das amostras que servirao para inicializar o modelo(homografia)
		% randomicamente no intervalo de 1..npts
		ind = randsample(npts, s);
		
		%inicializa o modelo
        M = feval(funcaoCriarModelo, x1(:,ind), x2(:,ind));
		
		
        [inliers] = feval(funcaoDist, M, x1, x2, t);
		
		
        
        % numero de inliers para este modelo
        ninliers = length(inliers);
		
		warning( sprintf('numero de Inliers %d', ninliers));
		
        if ninliers > largestConsensus		% Maior quantidade de inliers até agora
            largestConsensus = ninliers;
            bestinliers = inliers;
			
			
            fracinliers =  ninliers/npts;
            pNoOutliers = 1 -  fracinliers^s;
            pNoOutliers = max(eps, pNoOutliers);  % evitar divisao por -Inf
            pNoOutliers = min(1-eps, pNoOutliers);% evitar divisao por 0.
            N = log(1-p)/log(pNoOutliers);
        end
		
		sample_count = sample_count+1;
    end
    
	M = feval(funcaoCriarModelo, x1(:,bestinliers), x2(:,bestinliers));

	
% Funcoes auxiliares

function [inliers] = distHomografia2d(H, x1, x2, t);
	x2_ = normalise(H * x1);
	x1_ = normalise(H \ x2);

	x1 = normalise(x1);
	x2 = normalise(x2);
	
	d2 = sum((x1-x1_).^2)  + sum((x2-x2_).^2);
	inliers = find(abs(d2) < t);

function nx = normalise(x)
	nx(1,:) = x(1,:)./x(3,:);
	nx(2,:) = x(2,:)./x(3,:);
    nx(3,:) = 1;
